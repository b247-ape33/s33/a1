
  fetch('https://jsonplaceholder.typicode.com/todos')
  .then((response) => response.json())
  .then((data) => {
    

  
     let details = data.map((val) => {
   
        return val.title;
       })
             let num = new Array(details);
             console.log(num);
    });


     fetch('https://jsonplaceholder.typicode.com/todos/1/')
   .then(response => {return response.json()})
   .then(function(data) {console.log(data)})


     
  fetch('https://jsonplaceholder.typicode.com/todos', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            completed: false,
            title: 'Created to do list Item',
            userId:1
        })
    })
    .then((response) => response.json())
    .then((data) => console.log(data))


     fetch('https://jsonplaceholder.typicode.com/todos/1/', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
         body: JSON.stringify({
            dateCompleted: "Pending",
            title: 'to Update the my to do list with a different data structure',
            status: "Pending",
            userId:1
        })
    })
    .then((response) => response.json())
    .then((data) => console.log(data));


     fetch('https://jsonplaceholder.typicode.com/todos/1/', {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
         body: JSON.stringify({
            dateCompleted: "07/09/21",
            status: "Complte",
            title: 'delectus aut autem',
            userId:1
        })
    })
    .then((response) => response.json())
    .then((data) => console.log(data));